gsap.registerPlugin(ScrollTrigger);

//Scroll Based Animation
const initializeScrollAnim = (container) => {
  const transition2 = container.querySelectorAll(".transition2");
  const transition3 = container.querySelectorAll(".transition3");

  gsap.from(transition2, {
    scrollTrigger: {
      trigger: ".transition2",
      start: "top bottom-=50px",
      id: 1,
    },
    y: 50,
    opacity: 0,
    duration: 2,
    stagger: 0.3,
  });
  gsap.from(transition3, {
    scrollTrigger: {
      trigger: ".transition3",
      start: "top bottom-=200px",
      id: 2,
    },
    y: 50,
    opacity: 0,
    duration: 2,
    stagger: 0.7,
  });
};

//Animation on page transitions
const animationEnterHome = container => {
  const content = container.querySelector(".content");
  const stagger1 = container.querySelectorAll(".stagger1");
  const heroDesign = container.querySelector(".hero-design");
  const squareAnim = container.querySelectorAll(".square-anim");

  //Hero and Animation
  const tl = gsap.timeline({ defaults: { ease: "power4.out" } });

  //Hero area animation
  tl.from(content, {
    y: "-40%",
    opacity: 0,
    duration: 2,
  })
    .from(
      stagger1,
      {
        opacity: 0,
        y: -50,
        stagger: 0.3,
        duration: 2,
      },
      "-=0.75"
    )
    .from(
      heroDesign,
      {
        opacity: 0,
        y: 50,
        duration: 1,
      },
      "-=1.5"
    )
    .from(
      squareAnim,
      {
        stagger: 0.2,
        scale: 0.1,
        duration: 1,
      },
      "-=2"
    );
  
  return tl;
}

const animationLeaveHome = container => {
  const content = container.querySelector(".content");
  const stagger1 = container.querySelectorAll(".stagger1");
  const heroDesign = container.querySelector(".hero-design");
  const squareAnim = container.querySelectorAll(".square-anim");

  //Hero and leave Animation
  const tl = gsap.timeline({ defaults: { ease: "power4.out" } });

  //Hero area leave animation
  tl.to(stagger1, {
    opacity: 0,
    y: -50,
    stagger: 0.3,
    duration: 2,
  }).to(
    squareAnim,
    {
      stagger: 0.2,
      scale: 0.1,
      duration: 1,
    },
    "-=2"
  ).to(
      heroDesign,
      {
        opacity: 0,
        y: 50,
        duration: 1,
      },
      "-=2"
    ).to(content, {
    y: "-40%",
    opacity: 0,
    duration: 1,
  },"-=1.5")

  return tl;
}

export {initializeScrollAnim, animationEnterHome, animationLeaveHome}