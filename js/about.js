//About page enter and leave animations
const animationEnterAbout = (container) => {
  const image = container.querySelector(".image-wrapper");
  const staggerAnim = container.querySelectorAll(".stagger-anim");

  const tl = gsap.timeline();

  tl.from(image, {
    opacity: 0,
    y: -50,
    duration: 1.5,
  }).from(
    staggerAnim,
    {
      stagger: 0.2,
      y: -50,
      opacity: 0,
    },
    "-=1"
  );

  return tl;
};

const animationLeaveAbout = (container) => {
  const image = container.querySelector(".image-wrapper");
  const staggerAnim = container.querySelectorAll(".stagger-anim");

  const tl = gsap.timeline({ defaults: { duration: 1.2 } });
  tl.to(image, {
    opacity: 0,
    x: -300,
  }).to(
    staggerAnim,
    {
      stagger: 0.1,
      x: 300,
      opacity: 0,
    },
    "-=1"
  );

  return tl;
};

export {animationEnterAbout, animationLeaveAbout}