import {initializeScrollAnim, animationEnterHome, animationLeaveHome} from './homepage.js';
import {animationEnterAbout, animationLeaveAbout} from './about.js';

//Barba
const barbaTransition = () => {
    barba.init({
    transitions: [
        {
        name: "home",
        to: {
            namespace: ["home"],
        },
        once({ next }) {
            animationEnterHome(next.container);
            initializeScrollAnim(next.container);
        },
        //Leave about
        leave: ({ current }) => animationLeaveAbout(current.container),
        afterLeave({ current }) {
            current.container.remove();
        },
        //Enter home
        enter({ next }) {
            animationEnterHome(next.container);
        },
        afterEnter({ next }) {
            initializeScrollAnim(next.container);
        },
        },
        {
        name: "leave-home",
        to: {
            namespace: ["about"],
        },
        once({ next }) {
            animationEnterAbout(next.container);
        },
        leave: ({ current }) => animationLeaveHome(current.container),
        enter({ next }) {
            animationEnterAbout(next.container);
        },
        },
    ],
    });
}

export default barbaTransition;
